const createError = require('http-errors');
const express = require('express');
const path = require('path');
const express_graphql = require("express-graphql");
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const schema = require("./graphqlAPI/schema");
const connect = require("./database/connection");
const bodyParser = require("body-parser");
const multer = require('multer')
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
// app.use('/files', express.static('public'))
app.use(express.static(path.join(__dirname, "client", "build")))

//Handle file upload
const storage = multer.diskStorage({
      destination: function (req, file, cb) {
      cb(null, 'client/images')
    },
    filename: function (req, file, cb) {
      cb(null, "profile_" + req.param("userId") + path.extname(file.originalname) )
    }
})
const upload = multer({ storage: storage }).single('file')
app.post('/upload/:userId',function(req, res) {
     
  upload(req, res, function (err) {
         if (err instanceof multer.MulterError) {
             return res.status(500).json(err)
         } else if (err) {
             return res.status(500).json(err)
         }
    return res.status(200).send(req.file)

  })

});
//End file upload

app.use(
  "/graphql",
  express_graphql({
    schema: schema,
    graphiql: true
  })
);
connect();
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "client", "build", "index.html"));
});

module.exports = app;


export interface NotificationProps{
    level?: NotificationLevel,
    msg: String,
}
export enum NotificationLevel{
    INFO,
    WARNING,
    ERROR,
}
export interface PopupProps{
    show: boolean;
    title: string;
    msg: string;
    danger: boolean;
    okText: string;
    cancelText: string;
    onOK: () => void;
    onCancel: () => void;
};

export interface ChoreSearchFilter{
    userId: string;
    choreDefId: string;
    timeFrom: Date;
    timeTo: Date;
}
export interface ChoreStat{
    time: Date;
    user: string;
    userName: string,
    choreDef: string;
    points: number;
    day: string;
}
export interface User{
    id: string;
    name: string;
    picture: string;
    email: string;
}

export interface Location{
    id: string;
    name: string,
}
export interface ChoreAction{
    id: string;
    name: string;
    description: string;
    points: number;
}

export interface ChoreDef{
    id: string;
    name: string;
    description: string;
    icon: any; //because string won't do; fa requires an iconprop
    priority: number;
    availableChoreActions: ChoreAction[];
}

export interface Chore{
    id: string;
    time: Date;
    choreDef: ChoreDef;
    choreActions: ChoreAction[];
    user: User;
}

export interface GraphQLError {
    msg: string,
    code: number
  }
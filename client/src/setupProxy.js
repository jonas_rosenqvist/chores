const proxy = require("http-proxy-middleware");
module.exports = function(app) {
  app.use(proxy("/graphql", { target: "http://localhost:3001" }));
  app.use(proxy("/upload", { target: "http://localhost:3001" }));
  app.use(proxy("/download", { target: "http://localhost:3001" }));
  app.use(proxy("/files", { target: "http://localhost:3001" }));
};

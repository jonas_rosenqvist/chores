import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import {
  faRestroom,
  faUtensils,
  faBed,
  faTshirt,
  faCat,
  faBroom,
  faDumpster,
  faShoppingBasket,
  faUser,
  faUsers,
  faHippo,
  faUserFriends,
  faCalendarAlt,
  faDatabase,
  faUserPlus,
  faLocationArrow,
  faMarker,
  faTrash,
  faChevronRight,
  faAngleDoubleDown,
  faAngleDoubleRight,
  faAngleDown,
  faAngleRight,
  faAngleUp,
  faPlus,
  faChartPie,
  faTable,
  faTasks,
  faEdit,
  faToilet,
  faToiletPaper,
  faBath,
  faWrench,
  faCoffee,
  faFilter,
  faWineBottle,
  faMoneyBillAlt,
  faFolderOpen,
  faReceipt,
  faRecycle,
  faBaby,
  faFeather,
  faGlassCheers,
  faHamburger,
  faHammer,
  faKey,
  faLaptopMedical,
  faMortarPestle,
  faPaintRoller,
  faPeopleCarry,
  faShoppingCart,
  faTag,
  faTags,
  faTruck,
  faSignOutAlt,
  faCog,
} from "@fortawesome/free-solid-svg-icons";
import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Layout from "./components/Layout";
library.add(
  fab,
  faRestroom,
  faUtensils,
  faBed,
  faTshirt,
  faCat,
  faBroom,
  faDumpster,
  faShoppingBasket,
  faUser,
  faUsers,
  faHippo,
  faUserFriends,
  faCalendarAlt,
  faDatabase,
  faUserPlus,
  faLocationArrow,
  faMarker,
  faTrash,
  faChevronRight,
  faBed,
  faAngleDoubleDown,
  faAngleDoubleRight,
  faAngleDown,
  faAngleRight,
  faAngleUp,
  faPlus,
  faChartPie,
  faTable,
  faTasks,
  faEdit,
  faToilet,
  faToiletPaper,
  faBath,
  faWrench,
  faCoffee,
  faFilter,
  faWineBottle,
  faMoneyBillAlt,
  faFolderOpen,
  faReceipt,
  faRecycle,
  faBaby,
  faFeather,
  faGlassCheers,
  faHamburger,
  faHammer,
  faKey,
  faLaptopMedical,
  faMortarPestle,
  faPaintRoller,
  faPeopleCarry,
  faShoppingCart,
  faTag,
  faTags,
  faTruck,
  faSignOutAlt,
  faCog
);

export const getIcons = ():any => {
  var libraryAny: any = library;
  return Object.keys(libraryAny.definitions.fas);
}

export default class App extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Route
            path="/"
            render={props => (
              <>
                <Layout {...props} />
              </>
            )}
          />
        </BrowserRouter>
      </>
    );
  }
}


import { request } from "graphql-request";
import { GraphQLError, User, Location, Chore, ChoreDef, ChoreSearchFilter, ChoreStat } from "./types";

export const updateUser = async (user: User): Promise<User> => {
  const updateUser = `mutation{ updateUser(
    id: "${user.id}"
    name: "${user.name}", 
    email: "${user.email}",
    picture: "${user.picture}",
    ) {
      id
      name
      email
      picture
    }}`;

  try {
    const result = await request("/graphql", updateUser);
    return { ...result.updateUser };
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
};

export const deleteUser = async (id:string) => {
  const query = `mutation
  {
    deleteUser(
      id: "${id}")
      {
        ok,
        msg
      }
  }`
  try {
    const data = await request("/graphql", query);
    return data.deleteUser;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
}
export const createUser = async (user: User): Promise<User> => {
  const createUser = `mutation{ createUser(
    name: "${user.name}", 
    email: "${user.email}",
    picture: "${user.picture}",
    ) {
      id
      name
      email
      picture
    }}`;

  try {
    const result = await request("/graphql", createUser);
    return { ...result.createUser };
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
};
export const createChoreDef = async (choreDef: ChoreDef) => {
  const { name, description, icon, priority } = choreDef;
  const createChoreDef = `mutation {
    createChoreDef(
      name: "${name}"
      description: "${description}"
      icon: "${icon}"
      priority: ${priority}
    ){
      id
      name
      description
      icon
      priority
      availableChoreActions{
        id
        name
        description
        points
      }
    }
  }`
  try {
    const result = await request("/graphql", createChoreDef);
    return { ...result.createChoreDef };
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
}
export const updateChoreDef = async (choreDef:ChoreDef) => {
  const { id, name, description, icon, priority, availableChoreActions } = choreDef;
  const updateChoreDef = `mutation {
    updateChoreDef(
      id: "${id}"
      name: "${name}"
      description: "${description}"
      icon: "${icon}"
      priority: ${priority}
      availableChoreActions: ${JSON.stringify(availableChoreActions).replace(/"([^(")"]+)":/g, "$1:")}
    ){
      id
    }
  }`
  try {
    const result = await request("/graphql", updateChoreDef);
    return { ...result.updateChoreDef };
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
}
export const createChoreAction = async (name:string, description:string, points:number) => {
  const createChoreAction = `mutation{ createChoreAction(
    name: "${name}"
    description: "${description}"
    points: ${points}
    ){
      id
      name
      description
      points
    }
  }`;
  try {
    const result = await request("/graphql", createChoreAction);
    return { ...result.createChoreAction };
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
}
export const createChore = async (chore:Chore): Promise<Chore> => {
 const {choreActions, choreDef, time, user} = chore;
 const actionArray = choreActions.map(action => action.id)

  const createChore = `mutation{ createChore(
    time: ${Math.floor(time.getTime() / 1000)}
    user: "${user.id}"
    choreDef: "${choreDef.id}"
    choreActions: ${JSON.stringify(actionArray).replace(/"([^(")"]+)":/g, "$1:")}
    ) {
      id
    }}`;

  try {
    const result = await request("/graphql", createChore);
    return { ...result.createChore };
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
};

export const getLocations = async (): Promise<Location[]> => {
  const query = `{
    locations{
      id
      name
    }
  }`;
  try {
    const data = await request("/graphql", query);
    return data.locations;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
};

export const getUsers = async (): Promise<User[]> => {
  const query = `{
    users{
      id
      name
      picture
      email
    }
  }`;
  try {
    const data = await request("/graphql", query);
    return data.users;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
};
export const getUser = async (id: string): Promise<User> => {
  const query = `{
  users(id:"${id}"){
    id
    name
    picture
    email
  }
}`;
  let users;
  try {
    const data = await request("/graphql", query);
    users = data.users;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
  if (users.length === 0) {
    const error = { msg: "User does not exist", code: 404 };
    throw error;
  }
  return users[0];
};

export const deleteChore = async (id:string) => {
  const query = `mutation
  {
    deleteChore(
      id: "${id}")
      {
        ok,
        msg
      }
  }`
  try {
    const data = await request("/graphql", query);
    return data.deleteChore;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
}
export const getChoreStats = async (searchFilter:ChoreSearchFilter): Promise<ChoreStat[]> => {
  const {userId, choreDefId, timeFrom, timeTo } = searchFilter;
  const query = `
  {
    choreStats(
      userId: "${userId}"
      choreDefId: "${choreDefId}"
      timeFrom: ${Math.floor(timeFrom.getTime() / 1000)}
      timeTo: ${Math.floor(timeTo.getTime() / 1000)}
      ){
        time,
        user, 
        userName,
        choreDef,
        points,
        day
    }
  }
    `;
  try {
    const data = await request("/graphql", query);
    return data.choreStats;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
}
export const getChores = async (searchFilter:ChoreSearchFilter, limit:number): Promise<Chore[]> => {
  const {userId, choreDefId, timeFrom, timeTo } = searchFilter;
  const query = `
  {
    chores(
      userId: "${userId}"
      choreDefId: "${choreDefId}"
      timeFrom: ${Math.floor(timeFrom.getTime() / 1000)}
      timeTo: ${Math.floor(timeTo.getTime() / 1000)}
      limit: ${limit}
      ){
      id
      time
      choreDef{
        id
        name
        description
        icon
        priority
      }
      choreActions{
        id
        name
        description
        points
      }
      user{
        id
        name
      }
    }
  }
    `;
  try {
    const data = await request("/graphql", query);
    return data.chores;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
};

export const getChoreDefs = async (): Promise<ChoreDef[]> => {
  const query = `
  {
    choreDefs {
      id
      name
      description
      icon
      priority
      availableChoreActions{
        id
        name
        description
        points
      }
    }
  }
  `;
  try {
    const data = await request("/graphql", query);
    return data.choreDefs;
  } catch (err) {
    throw getErrorMsgAndStatus(err);
  }
};

const getErrorMsgAndStatus = (err: any): GraphQLError => {
  const error = { msg: err.response.error, code: err.response.status };
  console.log(error);
  return error;
};

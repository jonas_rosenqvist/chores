import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { updateUser, createUser, deleteUser } from "../GraphQL";
import { User, PopupProps, NotificationLevel } from "../types";

import { Container, Button, Row, Col, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Collapse } from "react-bootstrap";

type Props = {
  setLocation: (location: string) => void;
  onAfterLogin: (user: User) => void;
  showNotification: (msg: string, level?: NotificationLevel) => void;
  showPopup: (props: PopupProps) => void;
  hidePopup: () => void;
  users: User[];
  onUsersChange: () => void;
};
type State = {
  user: User;
  userFile: any;
  newUser: boolean;
  redirect: string;
};
class Users extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      user: {
        id: "",
        name: "",
        picture: "",
        email: ""
      },
      userFile: null,
      newUser: false,
      redirect: ""
    };
  }

  async componentDidMount() {
    this.props.setLocation("Users");
  }

  deleteUser = async () => {
    this.props.showPopup({
      show: true,
      title: "Remove user",
      msg:
        "Are you sure you want to remove this user? Removed users are hidden from the login view but his or her data remain.",
      danger: true,
      okText: "Remove user",
      cancelText: "Cancel",
      onOK: async () => {
        const { user } = this.state;
        if (user) {
          try {
            await deleteUser(user.id);
            this.props.onUsersChange();
            this.props.hidePopup();
            this.props.showNotification("User deleted", NotificationLevel.ERROR)
          } catch (error) {
            console.log(error);
          }
        }
      },
      onCancel: () => this.props.hidePopup()
    });
  };
  updateUser = async () => {
    const { userFile, user } = this.state;
    if (userFile) {
      const resp = await this.upload();
      const file = await resp.json();
      user.picture = file.filename;
    }
    try {
      await updateUser(user);
      this.setState({ user: this.getClearedUser() });
    } catch (error) {
      console.log(error);
    }
    this.props.onUsersChange();
  };

  getClearedUser = () => {
    return {
      id: "",
      name: "",
      email: "",
      picture: ""
    };
  };
  createUser = async () => {
    const { userFile, user } = this.state;
    if (userFile) {
      const resp = await this.upload();
      const file = await resp.json();
      user.picture = file.filename;
    }
    try {
      await createUser(user);
      this.setState({ user: this.getClearedUser(), newUser: false });
    } catch (error) {
      console.log(error);
    }
    this.props.onUsersChange();
  };

  upload = async () => {
    const data = new FormData();
    data.append("file", this.state.userFile);
    return await fetch("/upload/" + (this.state.user.id || "pending"), {
      method: "POST",
      body: data
    });
  };

  onLogin = (user: User) => {
    this.props.onAfterLogin(user);
    this.setState({ redirect: "/register/" });
  };

  render() {
    const { user: currentUser, redirect, newUser } = this.state;
    const { users } = this.props;
    if (redirect) {
      return <Redirect to={redirect} />;
    }
    return (
      <Container className="my-5">
        <Row>
          {users.map(user => (
            <Col xs={12} md={6} lg={4} key={user.id}>
              <Card
                className="my-2 mx-auto"
                bg="light"
                style={{ width: "18rem" }}
                key={user.id}
              >
                <Card.Header>
                  <Card.Title style={{ marginBottom: "-.25rem" }}>
                    {user.name}
                    {/* <img
                      className="img-fluid rounded-circle shadow"
                      style={{ maxWidth: "2rem" }}
                      src="/files/images/lucy.jpg"
                    /> */}
                  </Card.Title>
                </Card.Header>
                <Card.Body>
                  <Card.Subtitle></Card.Subtitle>
                  <Card.Link onClick={() => this.onLogin(user)}>
                    Login
                  </Card.Link>
                  <Card.Link
                    className="float-right"
                    onClick={() =>
                      this.setState(
                        currentUser.id !== user.id
                          ? {
                              newUser: false,
                              user
                            }
                          : {
                              newUser: false,
                              user: {
                                id: "",
                                name: "",
                                email: "",
                                picture: ""
                              }
                            }
                      )
                    }
                  >
                    Edit{" "}
                    <FontAwesomeIcon
                      icon={
                        currentUser.id === user.id ? "angle-up" : "angle-down"
                      }
                    />
                  </Card.Link>
                </Card.Body>
                <Collapse in={currentUser.id === user.id}>
                  <Card.Footer>
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">#</span>
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Username"
                        aria-label="Username"
                        defaultValue={currentUser.name}
                        onChange={e =>
                          this.setState({
                            user: {
                              ...this.state.user,
                              name: e.target.value
                            }
                          })
                        }
                      />
                    </div>
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">
                          @
                        </span>
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Email"
                        aria-label="Email"
                        aria-describedby="basic-addon1"
                        defaultValue={currentUser.email}
                        onChange={e =>
                          this.setState({
                            user: { ...this.state.user, email: e.target.value }
                          })
                        }
                      />
                    </div>
                    <div className="input-group mb-3">
                      <div className="custom-file">
                        <input
                          type="file"
                          className="custom-file-input"
                          id="inputGroupFile01"
                          aria-describedby="inputGroupFileAddon01"
                          onChange={(e: any) =>
                            this.setState({ userFile: e.target.files[0] })
                          }
                        />
                        <label
                          className="custom-file-label"
                          htmlFor="inputGroupFile01"
                        >
                          {this.state.userFile
                            ? "File selected"
                            : "Choose file"}
                        </label>
                      </div>
                    </div>
                    <Button
                      variant="info"
                      style={{ width: "100%" }}
                      onClick={this.updateUser}
                    >
                      Save user
                    </Button>
                    <Button
                      variant="danger"
                      className="mt-1"
                      style={{ width: "100%" }}
                      onClick={this.deleteUser}
                    >
                      Remove user
                    </Button>
                  </Card.Footer>
                </Collapse>
              </Card>
            </Col>
          ))}
          <Col xs={12} md={6} lg={4}>
            <Card
              className="my-2 mx-auto"
              bg="light"
              style={{ width: "18rem" }}
            >
              <Card.Header>
                <Card.Title
                  style={{ marginBottom: "-.25rem", fontStyle: "italic" }}
                >
                  New
                </Card.Title>
              </Card.Header>
              <Card.Body>
                <Card.Link
                  onClick={() =>
                    this.setState({
                      user: {
                        id: "",
                        name: "",
                        email: "",
                        picture: ""
                      },
                      newUser: !newUser
                    })
                  }
                >
                  Create a new user{" "}
                  <FontAwesomeIcon icon={newUser ? "angle-up" : "angle-down"} />
                </Card.Link>
              </Card.Body>
              <Collapse in={newUser}>
                <Card.Footer>
                  <div className="input-group mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">#</span>
                    </div>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Username"
                      aria-label="Username"
                      defaultValue={currentUser.name}
                      onChange={e =>
                        this.setState({
                          user: { ...this.state.user, name: e.target.value }
                        })
                      }
                    />
                  </div>
                  <div className="input-group mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text" id="basic-addon1">
                        @
                      </span>
                    </div>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Email"
                      aria-label="Email"
                      aria-describedby="basic-addon1"
                      defaultValue={currentUser.email}
                      onChange={e =>
                        this.setState({
                          user: { ...this.state.user, email: e.target.value }
                        })
                      }
                    />
                  </div>
                  <div className="input-group mb-3">
                    <div className="custom-file">
                      <input
                        type="file"
                        className="custom-file-input"
                        id="inputGroupFile01"
                        aria-describedby="inputGroupFileAddon01"
                        onChange={(e: any) =>
                          this.setState({ userFile: e.target.files[0] })
                        }
                      />
                      <label
                        className="custom-file-label"
                        htmlFor="inputGroupFile01"
                      >
                        {this.state.userFile ? "File selected" : "Choose file"}
                      </label>
                    </div>
                  </div>
                  <Button
                    variant="info"
                    style={{ width: "100%" }}
                    onClick={this.createUser}
                  >
                    Create
                  </Button>
                </Card.Footer>
              </Collapse>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Users;

import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import {
  getChoreDefs,
  updateChoreDef,
  createChoreDef,
  createChoreAction
} from "../GraphQL";

import {
  Container,
  Row,
  Col,
  ListGroup,
  Form,
  Button,
  InputGroup
} from "react-bootstrap";
import {
  GraphQLError,
  PopupProps,
  NotificationLevel,
  User,
  ChoreDef
} from "../types";
import { renderLoading, getPriorityOptions } from "./Snippets";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getIcons } from "../App";

type Props = {
  setLocation: (location: string) => void;
  showNotification: (msg: string, level?: NotificationLevel) => void;
  showPopup: (popupProps: PopupProps) => void;
  hidePopup: () => void;
  user: User;
  loadingUser: boolean;
};
type State = {
  redirect: string;
  error: GraphQLError | null;
  choreDefs: ChoreDef[];
  selectedChoreDef: ChoreDef;
};

class Define extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      redirect: "",
      error: null,
      choreDefs: [],
      selectedChoreDef: this.getEmptyChoreDef()
    };
  }
  async componentDidMount() {
    if (!this.props.user.id && !this.props.loadingUser) {
      this.setState({ redirect: "/users/" });
      return;
    }
    try {
      const choreDefs = await getChoreDefs();
      const selectedChoreDef =
        choreDefs.length > 0 ? choreDefs[0] : this.getEmptyChoreDef();
      this.setState({ choreDefs, selectedChoreDef });
    } catch (error) {
      this.setState({ error });
    }
    this.props.setLocation("Manage Chores");
  }

  deleteAction = async (index: number) => {
    this.props.showPopup({
      show: true,
      title: "Delete chore action",
      msg:
        "Are you sure you want to delete this chore action? This removes the possibility of registering this action in the future, but any earlier registration made on it remains in the database.",
      danger: true,
      okText: "Delete action",
      cancelText: "Cancel",
      onOK: async () => {
        this.props.hidePopup();
        const { selectedChoreDef } = this.state;
        selectedChoreDef.availableChoreActions.splice(index, 1);
        await this.updateOrCreateChoreDef(selectedChoreDef);
      },
      onCancel: () => {
        this.props.hidePopup();
      }
    });
  };
  moveActionUp = async (index: number) => {
    if (index === 0) {
      return;
    }
    const { availableChoreActions } = this.state.selectedChoreDef;
    const tmp = availableChoreActions[index];
    availableChoreActions[index] = availableChoreActions[index - 1];
    availableChoreActions[index - 1] = tmp;
    this.setState({
      selectedChoreDef: {
        ...this.state.selectedChoreDef,
        availableChoreActions
      }
    });
  };
  render() {
    const { error, choreDefs, redirect, selectedChoreDef } = this.state;
    if (!choreDefs) {
      return renderLoading();
    }
    if (redirect) {
      return <Redirect to={redirect} />;
    }
    if (error) {
      return "Error";
    }
    return (
      <Container className="my-5">
        <Row>
          <Col xs={12} md={6} lg={4} className="mb-5">
            <h4>Existing chore definition</h4>
            <ListGroup>
              {choreDefs.map((choreDef, index) => (
                <ListGroup.Item
                  key={choreDef.id}
                  active={selectedChoreDef.id === choreDef.id}
                  action
                  onClick={() => this.setState({ selectedChoreDef: choreDef })}
                >
                  {choreDef.name}
                </ListGroup.Item>
              ))}
              <ListGroup.Item
                active={!selectedChoreDef.id}
                action
                onClick={() =>
                  this.setState({ selectedChoreDef: this.getEmptyChoreDef() })
                }
              >
                <i>+ New chore definition</i>
              </ListGroup.Item>
            </ListGroup>
          </Col>
          <Col xs={12} md={6} lg={8}>
            <h4>{selectedChoreDef.name || <i>Untitled Chore</i>}</h4>
            <Form.Group>
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text style={{ width: "4em" }}>
                    Name
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  type="text"
                  placeholder="A short (one or two word) name of the chore"
                  required
                  defaultValue={selectedChoreDef.name}
                  onChange={(e: any) =>
                    this.setState({
                      selectedChoreDef: {
                        ...this.state.selectedChoreDef,
                        name: e.target.value
                      }
                    })
                  }
                />
              </InputGroup>
            </Form.Group>
            <Form.Group>
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text style={{ width: "4em" }}>
                    Desc.
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  as="textarea"
                  className="form-control"
                  rows={3}
                  placeholder="A more detailed description of the chore"
                  value={selectedChoreDef.description}
                  onChange={(e: any) =>
                    this.setState({
                      selectedChoreDef: {
                        ...this.state.selectedChoreDef,
                        description: e.target.value
                      }
                    })
                  }
                />
              </InputGroup>
            </Form.Group>
            <Row>
              <Col xs={12}>
                <Form.Group>
                  <Form.Label>This chore will get registrered:</Form.Label>
                  <Form.Control
                    as="select"
                    value={selectedChoreDef.priority.toString()}
                    onChange={(e: any) =>
                      this.setState({
                        selectedChoreDef: {
                          ...this.state.selectedChoreDef,
                          priority: parseInt(e.target.value)
                        }
                      })
                    }
                  >
                    {getPriorityOptions()}
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col style={{ fontSize: "1.3em" }}>Icon</Col>
            </Row>
            <div className="icon-card-container">
              <Row>
                {getIcons().map((icon: any) => {
                  const cardClass =
                    icon === selectedChoreDef.icon
                      ? "icon-card icon-card-active"
                      : "icon-card";
                  return (
                    <Col xs={2} key={icon} style={{ padding: "0.2em" }}>
                      <div
                        className={cardClass}
                        title={icon}
                        onClick={() =>
                          this.setState({
                            selectedChoreDef: {
                              ...this.state.selectedChoreDef,
                              icon
                            }
                          })
                        }
                      >
                        <FontAwesomeIcon icon={icon} />
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </div>
            {selectedChoreDef.id && (
              <Row className="mt-5">
                <Col xs={7} style={{ fontSize: "1.3em" }}>
                  {selectedChoreDef.availableChoreActions.length > 0 ? (
                    "Available actions"
                  ) : (
                    <i>No available actions</i>
                  )}
                </Col>
                <Col
                  xs={5}
                  onClick={this.createNewAction}
                  className="text-right"
                >
                  <Button
                    style={{ padding: "0", fontSize: "1.2em" }}
                    variant="link"
                    disabled={!selectedChoreDef.id}
                    title="Create a new action for this chore"
                  >
                    New action
                  </Button>
                </Col>
              </Row>
            )}
            <Row>
              <Col className="mt-1">
                {selectedChoreDef.availableChoreActions.map((action, index) => {
                  return (
                    <div key={action.id}>
                      <div>
                        <span style={{ fontWeight: "bold" }}>
                          {action.name || "New action"}
                        </span>
                        <div
                          title="Move this action up"
                          style={{
                            display: "inline-block",
                            cursor: "pointer",
                            marginLeft: "1em",
                            color: "#117a8b"
                          }}
                          onClick={() => this.moveActionUp(index)}
                        >
                          <FontAwesomeIcon icon="angle-up" />
                        </div>
                        <div
                          title="Delete this action"
                          style={{
                            display: "inline-block",
                            float: "right",
                            cursor: "pointer",
                            color: "#b31c2a"
                          }}
                          onClick={() => this.deleteAction(index)}
                        >
                          Delete
                        </div>
                      </div>

                      <Form.Group>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text style={{ width: "4em" }}>
                              Name
                            </InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control
                            type="text"
                            className="form-control"
                            placeholder="The name of the action"
                            defaultValue={action.name}
                            onChange={(e: any) =>
                              this.editSelectedChoreDefAction(
                                action.id,
                                "name",
                                e.target.value
                              )
                            }
                          />
                        </InputGroup>
                      </Form.Group>
                      <Form.Group>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text style={{ width: "4em" }}>
                              Desc.
                            </InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control
                            type="text"
                            className="form-control"
                            placeholder="Description"
                            defaultValue={action.description}
                            onChange={(e: any) =>
                              this.editSelectedChoreDefAction(
                                action.id,
                                "description",
                                e.target.value
                              )
                            }
                          />
                        </InputGroup>
                      </Form.Group>
                      <Form.Group>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text style={{ width: "4em" }}>
                              Points
                            </InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control
                            type="number"
                            className="form-control"
                            placeholder="The number of points awarded for this task"
                            defaultValue={
                              action.points ? action.points.toString() : "0"
                            }
                            onChange={(e: any) =>
                              this.editSelectedChoreDefAction(
                                action.id,
                                "points",
                                e.target.value
                              )
                            }
                          />
                        </InputGroup>
                      </Form.Group>
                    </div>
                  );
                })}
                <Button
                  className="mt-3 mb-5"
                  onClick={() =>
                    this.updateOrCreateChoreDef(this.state.selectedChoreDef)
                  }
                  size={"lg"}
                  variant="info"
                  style={{ width: "100%" }}
                >
                  {selectedChoreDef.id ? "Save" : "Create"}{" "}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
  //TODO: bug, after api call both selectedChoreDef AND choredefs[correctIndex] needs to be updated
  updateOrCreateChoreDef = async (choreDef: ChoreDef) => {
    if (!choreDef.name.trim()) {
      this.props.showNotification("Name is required", NotificationLevel.ERROR);
      return;
    }
    if (choreDef.id) {
      let validActions = true;
      choreDef.availableChoreActions.forEach(action => {
        if (!action.name.trim()) {
          this.props.showNotification(
            "Name is required on actions",
            NotificationLevel.ERROR
          );
          validActions = false;
          return;
        }
        if (isNaN(action.points)) {
          this.props.showNotification(
            "Points must be given a numeric value",
            NotificationLevel.ERROR
          );
          validActions = false;
          return;
        }
      });
      if (!validActions) {
        return;
      }
      await updateChoreDef(choreDef);
      this.props.showNotification("Saved!", NotificationLevel.INFO);
    } else {
      const created = await createChoreDef(choreDef);
      const { choreDefs } = this.state;
      choreDefs.push(created);
      this.setState({
        choreDefs,
        selectedChoreDef: { ...this.state.selectedChoreDef, id: created.id }
      });
      this.props.showNotification("Created!");
    }
  };
  editSelectedChoreDefAction = (
    choreDefActionId: string,
    fieldName: string,
    value: string
  ) => {
    const actions = this.state.selectedChoreDef.availableChoreActions;
    const index = actions.map(action => action.id).indexOf(choreDefActionId);
    switch (fieldName) {
      case "name":
        actions[index].name = value;
        break;
      case "description":
        actions[index].description = value;
        break;
      case "points":
        actions[index].points = parseInt(value);
    }

    this.setState({
      selectedChoreDef: {
        ...this.state.selectedChoreDef,
        availableChoreActions: actions
      }
    });
  };
  /*
  Create a new empty choredef in the db, and adds it to the selectedChoreDef.
  */
  createNewAction = async () => {
    const choreAction = await createChoreAction("", "", 10);
    const actions = this.state.selectedChoreDef.availableChoreActions;
    actions.unshift(choreAction);
    this.setState({
      selectedChoreDef: {
        ...this.state.selectedChoreDef,
        availableChoreActions: actions
      }
    });
  };
  getEmptyChoreDef = () => {
    return {
      id: "",
      name: "",
      description: "",
      icon: "",
      priority: 2,
      availableChoreActions: []
    };
  };
}
export default Define;

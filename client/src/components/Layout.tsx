import React, { Component } from "react";
import { Route, Link, Redirect } from "react-router-dom";
import Cookies from "universal-cookie";
import { getUser, getUsers } from "../GraphQL";
import {
  GraphQLError,
  User,
  PopupProps,
  NotificationLevel,
  NotificationProps
} from "../types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Users from "./Users";
import Notify from "./Notify";
import { Navbar, Nav } from "react-bootstrap";
import Register from "./Register";
import Define from "./Define";
import Visualize from "./Visualize";
import Popup from "./Popup";

type Props = {};
type State = {
  user: User;
  users: User[];
  error: GraphQLError | null;
  location: string;
  loadingUser: boolean;
  popup: PopupProps;
  notification: NotificationProps;
  navBarExpanded: boolean;
};
class Layout extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      user: {
        id: "",
        name: "",
        email: "",
        picture: ""
      },
      users: [],
      error: null,
      location: "",
      loadingUser: true,
      popup: {
        show: false,
        title: "",
        msg: "",
        danger: false,
        okText: "",
        cancelText: "",
        onOK: () => {},
        onCancel: () => {}
      },
      notification: { msg: "", level: NotificationLevel.INFO },
      navBarExpanded: false
    };
  }

  async componentDidMount() {
    const cookies = new Cookies();
    const userId = cookies.get("userId");
    if (userId && !this.state.user.id) {
      this.setState({ loadingUser: true });
      try {
        const user = await getUser(userId);
        this.setState({ user, loadingUser: false });
      } catch (error) {
        this.setState({ error });
      }
    } else {
      this.setState({ loadingUser: false });
    }
    this.reloadUsers();
  }
  setLocation = (location: string) => {
    this.setState({ location });
  };
  onAfterLogin = (user: User) => {
    const cookies = new Cookies();
    this.setState({ user });
    cookies.set("userId", user.id, {
      path: "/"
    });
  };
  showNotification = (msg: string, level?: NotificationLevel) => {
    this.setState({
      notification: { msg, level: level || NotificationLevel.INFO }
    });
    setTimeout(
      () =>
        this.setState({
          notification: { msg: "", level: NotificationLevel.INFO }
        }),
      3000
    );
  };
  showPopup = (popupProps: PopupProps) => {
    const {
      show,
      title,
      msg,
      danger,
      okText,
      cancelText,
      onOK,
      onCancel
    } = popupProps;
    this.setState({
      popup: {
        show,
        title,
        msg,
        danger,
        okText,
        cancelText,
        onOK,
        onCancel
      }
    });
  };
  hidePopup = () => {
    this.setState({
      popup: {
        ...this.state.popup,
        show: false
      }
    });
  };
  reloadUsers = async () => {
    const users = await getUsers();
    this.setState({ users });
  };
  render() {
    const {
      user,
      location,
      loadingUser,
      notification,
      users,
      popup
    } = this.state;
    return (
      <>
        {popup.show && <Popup {...popup} />}
        <Navbar
          bg="light"
          expand="lg"
          expanded={this.state.navBarExpanded}
          onToggle={(expanded: any) =>
            this.setState({ navBarExpanded: expanded })
          }
        >
          <Navbar.Brand>
            {" "}
            Chores{" "}
            <small>
              <FontAwesomeIcon icon="angle-double-right" /> {location}
            </small>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Link
                className="nav-link"
                to="/users/"
                onClick={() => this.setState({ navBarExpanded: false })}
              >
                <FontAwesomeIcon icon="user-friends" /> Users
              </Link>
              {user.id && (
                <>
                  <Link
                    className="nav-link"
                    to="/register/"
                    onClick={() => this.setState({ navBarExpanded: false })}
                  >
                    <FontAwesomeIcon icon="tasks" /> Register Chore
                  </Link>
                  <Link
                    className="nav-link"
                    to="/define/"
                    onClick={() => this.setState({ navBarExpanded: false })}
                  >
                    <FontAwesomeIcon icon="edit" /> Manage Chores
                  </Link>
                  <Link
                    className="nav-link"
                    to="/visualize/"
                    onClick={() => this.setState({ navBarExpanded: false })}
                  >
                    <FontAwesomeIcon icon="edit" /> Data Visualization
                  </Link>
                </>
              )}
            </Nav>
            {user.name && (
              <Nav className="mr-0">
                <div className="nav-link">Logged in as {user.name}</div>
                <Link
                  className="nav-link"
                  to="/users/"
                  onClick={() =>
                    this.setState({
                      user: { id: "", name: "", email: "", picture: "" }
                    })
                  }
                >
                  <FontAwesomeIcon icon="sign-out-alt" /> Logout
                </Link>
              </Nav>
            )}
          </Navbar.Collapse>
        </Navbar>
        <Notify notificationProps={notification}></Notify>
        <Route
          path="/users/*"
          exact={true}
          render={props => (
            <Users
              setLocation={this.setLocation}
              showNotification={this.showNotification}
              showPopup={this.showPopup}
              hidePopup={this.hidePopup}
              onAfterLogin={this.onAfterLogin}
              onUsersChange={this.reloadUsers}
              users={users}
              {...props}
            />
          )}
        />
        <Route
          path="/visualize/*"
          exact={true}
          render={props => (
            <Visualize
              setLocation={this.setLocation}
              showNotification={this.showNotification}
              showPopup={this.showPopup}
              hidePopup={this.hidePopup}
              loadingUser={loadingUser}
              user={user}
              users={users}
              {...props}
            />
          )}
        />
        <Route
          path="/register/*"
          exact={true}
          render={props => (
            <Register
              setLocation={this.setLocation}
              showNotification={this.showNotification}
              showPopup={this.showPopup}
              hidePopup={this.hidePopup}
              loadingUser={loadingUser}
              user={user}
              {...props}
            />
          )}
        />
        <Route
          path="/define/*"
          exact={true}
          render={props => (
            <Define
              setLocation={this.setLocation}
              showNotification={this.showNotification}
              showPopup={this.showPopup}
              hidePopup={this.hidePopup}
              loadingUser={loadingUser}
              user={user}
              {...props}
            />
          )}
        />
        {/* root path: redirect to register if logged in, users o.w */}
         <Route
          path="/"
          exact={true}
          render={props => (
            <Redirect to={user.id ? "users/" : "register/"}/>
          )}
        />
      </>
    );
  }
}

export default Layout;

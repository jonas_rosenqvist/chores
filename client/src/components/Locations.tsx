import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { getLocations } from "../GraphQL";
import { User, PopupProps, NotificationLevel } from "../types";
import { Container, Row, Col } from "react-bootstrap";

type Props = {
  setLocation: (location: string) => void;
  showNotification: (msg: string, level?: NotificationLevel) => void;
  showPopup: (popupProps: PopupProps) => void;
  hidePopup: () => void;
  user: User;
  loadingUser: boolean;
};
type State = {
  redirect: string;
  error: string;
  locations: any;
};

class Locations extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      redirect: "",
      error: "",
      locations: null
    };
  }
  async componentDidMount() {
    this.props.setLocation("Locations");
    const locations = await getLocations();
    this.setState({ locations });
  }

  render() {
    const { error, locations, redirect } = this.state;
    if (!locations) {
      return <div>Loading..</div>;
    }
    if (redirect) {
      return <Redirect to={redirect} />;
    }
    if (error) {
      return "Error";
    }
    return (
      <Container className="my-5">
        <Row>
          {locations.map((location: any) => (
            <Col xs={12} md={6} lg={4} key={location.id}>
              {location.name}
            </Col>
          ))}
        </Row>
      </Container>
    );
  }
}
export default Locations;

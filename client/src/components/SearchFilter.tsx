import React, { Component } from "react";
import {
  FormGroup,
  Form,
  FormControl,
  Button,
  Collapse
} from "react-bootstrap";
import { ChoreSearchFilter, ChoreDef, User, Chore, ChoreStat } from "../types";
import moment from "moment";
import { getChores, getChoreDefs, getUsers, getChoreStats } from "../GraphQL";

type Props = {
  onSearchComplete: (chores: Chore[], choreStats:ChoreStat[]) => void;
  onSearchStarting: () => void;
  limit: number;
};

type State = {
  filter: ChoreSearchFilter;
  filterType: string;
  lastNdays: number;
  users: User[];
  choreDefs: ChoreDef[];
  chores: Chore[];
  choreStats: ChoreStat[];
  error: string;
};
export const defaultSearchFilter = () => {
  return {
    userId: "",
    choreDefId: "",
    timeFrom: moment()
      .subtract(1, "month")
      .startOf("day")
      .toDate(),
    timeTo: moment()
      .endOf("day")
      .toDate()
  };
};
class SearchFilter extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      filter: defaultSearchFilter(),
      filterType: "isoWeek",
      lastNdays: 7,
      users: [],
      choreDefs: [],
      chores: [],
      choreStats: [],
      error: ""
    };
  }
  async componentDidMount() {
    try {
      const choreDefs = await getChoreDefs();
      const users = await getUsers();
      this.setState({ choreDefs, users });
      await this.search();
    } catch (error) {
      this.setState({ error });
    }
  }

  async componentDidUpdate(prevProps: Props) {
    if (prevProps.limit !== this.props.limit) {
      this.search();
    }
  }

  setFilterType = (filterType: any) => {
    if (filterType === "advanced") {
      this.setState({ filterType: "advanced" });
      return;
    }
    let timeFrom;
    if (filterType === "allTime") {
      timeFrom = moment("2000-01-01", "YYYY-MM-DD").toDate();
    } else if (filterType === "lastNdays"){
      timeFrom = moment().subtract(this.state.lastNdays, "day").startOf("day").toDate();
    }else {
      timeFrom = moment()
        .startOf(filterType)
        .toDate();
    }
    this.setState({
      filterType,
      filter: {
        ...this.state.filter,
        timeFrom,
        timeTo: moment().toDate(),
        userId: "",
        choreDefId: ""
      }
    });
  };
  search = async () => {
    this.props.onSearchStarting();
    const chores = await getChores(this.state.filter, this.props.limit);
    const choreStats = await getChoreStats(this.state.filter);
    this.setState({ chores, choreStats });
    this.props.onSearchComplete(chores, choreStats);
  };
  render() {
    const { users, choreDefs, filter } = this.state;
    const { timeFrom, timeTo } = filter;
    const validTimeFrom = moment(timeFrom, "YYYY-MM-DD").isValid();
    const validTimeTo = moment(timeTo, "YYYY-MM-DD").isValid();

    return (
      <Form>
        <Form.Control
          as="select"
          value={this.state.filterType}
          className="mb-3"
          onChange={(e: any) => this.setFilterType(e.target.value)}
        >
          <option value="advanced">Advanced search</option>
          <option value="lastNdays">Last N days</option>
          <option value="day">Today</option>
          <option value="isoWeek">This week</option>
          <option value="month">This month</option>
          <option value="year">This year</option>
          <option value="allTime">All time</option>
        </Form.Control>
        <Collapse in={this.state.filterType === "lastNdays"}>
        <div>
            <FormGroup>
              <Form.Label>Since days ago</Form.Label>
              <Form.Control
                type="number"
                value={this.state.lastNdays.toString()}
                onChange={(e: any) =>
                  this.setState({...this.state, 
                     lastNdays: parseInt(e.target.value),
                     filter: {...this.state.filter, timeFrom: moment().subtract(parseInt(e.target.value), "day").startOf("day").toDate()}
                  })
                }
              >
              </Form.Control>
            </FormGroup>
            </div>
        </Collapse>
        <Collapse in={this.state.filterType === "advanced"}>
          <div>
            <FormGroup>
              <Form.Label>User</Form.Label>
              <Form.Control
                as="select"
                value={this.state.filter.userId}
                onChange={(e: any) =>
                  this.setState({
                    filter: { ...this.state.filter, userId: e.target.value }
                  })
                }
              >
                <option value={""}>Any</option>
                {users.map(user => (
                  <option key={user.id} value={user.id}>
                    {user.name}
                  </option>
                ))}
              </Form.Control>
            </FormGroup>
            <Form.Group>
              <Form.Label>Chore</Form.Label>
              <Form.Control
                as="select"
                value={this.state.filter.choreDefId}
                onChange={(e: any) =>
                  this.setState({
                    filter: {
                      ...this.state.filter,
                      choreDefId: e.target.value
                    }
                  })
                }
              >
                <option value={""}>Any</option>
                {choreDefs.map(choreDef => (
                  <option key={choreDef.id} value={choreDef.id}>
                    {choreDef.name}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
            <FormGroup>
              <Form.Label>Registered between</Form.Label>
              <FormControl
                type="date"
                defaultValue={moment(this.state.filter.timeFrom).format("YYYY-MM-DD")}
                onChange={(e: any) =>
                  this.setState({
                    filter: {
                      ...this.state.filter,
                      timeFrom: moment(e.target.value, "YYYY-MM-DD")
                        .startOf("day")
                        .toDate()
                    }
                  })
                }
              />
              {!validTimeFrom && (
                <div style={{ color: "red" }}>This date is invalid</div>
              )}
              and
              <FormControl
                type="date"
                defaultValue={moment(this.state.filter.timeTo).format("YYYY-MM-DD")}
                onChange={(e: any) =>
                  this.setState({
                    filter: {
                      ...this.state.filter,
                      timeTo: moment(e.target.value, "YYYY-MM-DD")
                        .endOf("day")
                        .toDate()
                    }
                  })
                }
              />
              {!validTimeTo && (
                <div style={{ color: "red" }}>This date is invalid</div>
              )}
            </FormGroup>
          </div>
        </Collapse>
        <Button
          variant="info"
          size={"lg"}
          disabled={!validTimeTo || !validTimeFrom}
          onClick={() => this.search()}
          style={{ width: "100%" }}
        >
          Search
        </Button>
      </Form>
    );
  }
}
export default SearchFilter;

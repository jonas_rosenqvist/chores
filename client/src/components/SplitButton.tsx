import React, { Component } from "react";

import {
  Button,
  Dropdown,
  ButtonGroup,
} from "react-bootstrap";

type Props = {
  uniqueButtonId: string;
  selectValues: string[];
  defaultValue: string;
  onClick: (selectedValue: string) => void;
  buttonText: string;
  disabled:boolean;
};
type State = {
  selectedValue: string;
};

export default class SplitButton extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { selectedValue: this.props.defaultValue };
  }
  render() {
    const { selectValues, onClick, buttonText, uniqueButtonId, disabled } = this.props;
    const { selectedValue } = this.state;
    return (
      <Dropdown style={{ width: "100%" }} as={ButtonGroup}>
        <Button
         disabled={disabled}
          style={{ width: "85%", padding: "0.5em", textAlign: "left", paddingLeft: "1em"}}
          variant={"info"}
          onClick={() => onClick(selectedValue)}
        >
          {`${buttonText} ${selectedValue.toLowerCase()}`}
        </Button>
        <Dropdown.Toggle
        disabled={disabled}
          split
          style={{ width: "15%" }}
          variant={"info"}
          id={uniqueButtonId}
        />
        <Dropdown.Menu className="dropdown-menu">
          {selectValues.map(value => {
            return (
              <Dropdown.Item
                key={value}
                className="dropdown-item"
                active={this.state.selectedValue === value}
                onClick={(e: any) =>
                  this.setState({ selectedValue: e.currentTarget.textContent })
                }
              >
                {value}
              </Dropdown.Item>
            );
          })}
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}

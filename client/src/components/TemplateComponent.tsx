import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { getUsers } from "../GraphQL";
import { User, NotificationLevel } from "../types";

import { Container, Button, Row, Col, Card } from "react-bootstrap";
import { GraphQLError } from "../types";

type Props = {
  setLocation: (location: string) => void;
  showNotification: (msg: string, level?: NotificationLevel) => void;
  showPopup: (
    title: string,
    msg: string,
    danger: boolean,
    okText: string,
    cancelText: string,
    onOK: () => void,
    onCancel: () => void
  ) => void;
  hidePopup: () => void;
  user: User;
  loadingUser: boolean;
};
type State = {
  redirect: string;
  error: GraphQLError | null;
  users: any[];
};

class Locations extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      redirect: "",
      error: null,
      users: []
    };
  }
  async componentDidMount() {
    if (!this.props.user.id && !this.props.loadingUser) {
      this.setState({ redirect: "/users/" });
      return;
    }
    try {
      const users = await getUsers();
      this.setState({ users });
    } catch (error) {
      this.setState({ error });
    }
    this.props.setLocation("Users");
  }

  render() {
    const { error, users, redirect } = this.state;
    if (!users) {
      return <div>Loading..</div>;
    }
    if (redirect) {
      return <Redirect to={redirect} />;
    }
    if (error) {
      return "Error";
    }
    return (
      <Container className="my-5">
        <Row>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}
export default Locations;

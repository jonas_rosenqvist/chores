import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { getChoreDefs, deleteChore } from "../GraphQL";
import TimeAgo from "react-timeago";
import {
  User,
  Chore,
  ChoreAction,
  ChoreDef,
  ChoreSearchFilter,
  ChoreStat,
  PopupProps
} from "../types";
import { Table, Button, Form, Collapse } from "react-bootstrap";
import moment from "moment";
import { Container, Row, Col } from "react-bootstrap";
import { GraphQLError, NotificationLevel } from "../types";
import { renderLoading, renderError } from "./Snippets";
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  BarChart,
  Bar,
  LineChart,
  Line
} from "recharts";
import SearchFilter from "./SearchFilter";

const lineColors = [
  "#7cc4cc",
  "#c27ecf",
  "#8582d1",
  "#82d190",
  "#d1829b",
  "#d1c382"
];
type Props = {
  setLocation: (location: string) => void;
  showNotification: (msg: string, level?: NotificationLevel) => void;
  showPopup: (props: PopupProps) => void;
  hidePopup: () => void;
  user: User;
  users: User[];
  loadingUser: boolean;
};
type State = {
  redirect: string;
  error: GraphQLError | null;
  chores: Chore[];
  choreDefs: ChoreDef[];
  choreStats: ChoreStat[];
  searching: boolean;
  filter: ChoreSearchFilter;
  limit: number;
  showTable: boolean;
  loading: boolean;
};

class Visualize extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      redirect: "",
      error: null,
      chores: [],
      choreStats: [],
      choreDefs: [],
      searching: true,
      loading: true,
      filter: {
        userId: "",
        choreDefId: "",
        timeFrom: moment()
          .subtract(1, "month")
          .startOf("day")
          .toDate(),
        timeTo: moment()
          .endOf("day")
          .toDate()
      },
      limit: 3,
      showTable: true
    };
  }
  async componentDidMount() {
    if (!this.props.user.id && !this.props.loadingUser) {
      this.setState({ redirect: "/users/" });
      return;
    }
    try {
      this.setState({ loading: true, searching: true });
      const choreDefs = await getChoreDefs();
      // const chores = await getChores(this.state.filter, this.state.limit);
      this.setState({
        // chores,
        choreDefs,
        loading: false,
        searching: false
      });
    } catch (error) {
      this.setState({ error, loading: false, searching: false });
    }
    this.props.setLocation("Table view");
  }

  searchComplete = async (chores: Chore[], choreStats: ChoreStat[]) => {
    this.setState({ chores, choreStats, searching: false });
  };

  deleteChoreAndUpdateState = async (choreid: string) => {
    this.props.showPopup({
      show: true,
      title: "Delete chore",
      msg:
        "Are you sure you want to delete this chore? This action cannot be undone",
      danger: true,
      okText: "Delete chore",
      cancelText: "Cancel",
      onOK: async () => {
        const result = await deleteChore(choreid);
        if (result.ok) {
          const updatedChores = this.state.chores.filter(
            chore => chore.id !== choreid
          );
          this.props.showNotification("Chore deleted", NotificationLevel.ERROR);
          this.setState({ chores: updatedChores });
          this.props.hidePopup();
        } else {
          renderError("The chore could not be deleted: " + result.msg);
          this.props.hidePopup();
        }
      },
      onCancel: () => {
        this.props.hidePopup();
      }
    });
  };

  render() {
    const {
      error,
      chores,
      redirect,
      choreDefs,
      choreStats,
      searching,
      limit,
      showTable,
      loading
    } = this.state;
    const { users } = this.props;
    if (redirect) {
      return <Redirect to={redirect} />;
    }
    if (loading) {
      return renderLoading();
    }
    if (choreDefs.length === 0 || users.length === 0) {
      return renderLoading("No chores has been registered yet");
    }
    if (error) {
      return "Error";
    }
    //Prep data for userSummary
    const tmp: any = {};
    choreStats.forEach(stat => {
      tmp[stat.userName] = (tmp[stat.userName] || 0) + stat.points;
    });
    const usersSummaryData = Object.keys(tmp).map(user => {
      return { name: user, points: tmp[user] };
    });

    //Prep data for usertTimeline
    const tmp2: any = {};
    choreStats.forEach(stat => {
      if (!tmp2[stat.day]) {
        tmp2[stat.day] = {};
      }
      tmp2[stat.day][stat.userName] = stat.points;
      //make sure every user has a datapoint for this date, even if 0
      users.forEach(user => {
        if (!tmp2[stat.day][user.name]) {
          tmp2[stat.day][user.name] = 0;
        }
      });
      tmp2[stat.day]["timestamp"] = stat.time;
    });
    const usersTimelineData = Object.keys(tmp2)
      .map(day => {
        return { day, ...tmp2[day] };
      })
      .sort((a, b) => Date.parse(a.timestamp) - Date.parse(b.timestamp));

    return (
      <Container className="mt-4">
        <Row>
          <Col xs={12} md={4} lg={3}>
            <h5 className="mt-3">Search filters</h5>
            <SearchFilter
              limit={limit}
              onSearchStarting={() => this.setState({ searching: true })}
              onSearchComplete={(chores: Chore[], choreStats: ChoreStat[]) =>
                this.searchComplete(chores, choreStats)
              }
            />
          </Col>
          {searching ? (
            <Col>
              {" "}
              <h5 className="mt-3"> Searching..</h5>
            </Col>
          ) : chores.length === 0 ? (
            <Col>
              <h5 className="mt-3">No matching results</h5>
            </Col>
          ) : (
            <Col xs={12} md={8} lg={9}>
              <Row>
                <Col xs={12} md={6}>
                  <Button
                    variant="link"
                    onClick={() => this.setState({ showTable: !showTable })}
                  >
                    {showTable ? "Hide table" : "Show table"}
                  </Button>
                </Col>
                <Col xs={12} md={6} className="text-right mb-2">
                  <div>
                    <Form.Control
                      as="select"
                      style={{ maxWidth: "20em", float: "right" }}
                      value={this.state.limit.toString()}
                      onChange={(e: any) =>
                        this.setState({
                          limit: parseInt(e.target.value)
                        })
                      }
                    >
                      <option value="3">Show the latest 3 entires</option>
                      <option value="10">Show the latest 10 entires</option>
                      <option value="50">Show the latest 50 entires</option>
                      <option value="100">Show the latest 100 entires</option>
                    </Form.Control>
                  </div>
                </Col>
                <Col>
                  <Collapse in={this.state.showTable}>
                    <Table size="sm" striped bordered responsive>
                      <thead>
                        <tr>
                          <th>User</th>
                          <th>Time</th>
                          <th>Chore</th>
                          <th>Actions</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        {chores.map(chore => (
                          <tr key={chore.id}>
                            <td>{chore.user.name}</td>
                            <td>
                              <TimeAgo date={chore.time} />
                              <div style={{ color: "#888" }}>
                                {moment(chore.time).format("YYYY-MM-DD HH:mm")}
                              </div>
                            </td>
                            <td>{chore.choreDef.name}</td>
                            <td>
                              <ul style={{ paddingLeft: "1em" }}>
                                {chore.choreActions.map(
                                  (action: ChoreAction) => (
                                    <li key={chore.id + action.id}>
                                      {action.name} <b>({action.points})</b>
                                    </li>
                                  )
                                )}
                              </ul>
                            </td>
                            <td>
                              {chore.user.id === this.props.user.id && (
                                <Button
                                  variant="link"
                                  onClick={() => {
                                    this.deleteChoreAndUpdateState(chore.id);
                                  }}
                                >
                                  Delete
                                </Button>
                              )}
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </Collapse>
                </Col>
              </Row>
              <Row>
                {usersSummaryData.length > 0 && (
                  <>
                    <Col>
                      <ResponsiveContainer width="100%" height={400}>
                        <BarChart data={usersSummaryData}>
                          <CartesianGrid strokeDasharray="3 3" />
                          <XAxis dataKey="name" />
                          <YAxis width={20} />
                          <Tooltip />
                          <Legend />
                          <Bar dataKey="points" fill="#17a2b8" />
                        </BarChart>
                      </ResponsiveContainer>
                    </Col>
                  </>
                )}
              </Row>
              <Row>
                {/* Looks ugly with less than 3 data points */}
                {usersTimelineData.length > 2 && (
                  <>
                    <Col>
                      <ResponsiveContainer width="100%" height={400}>
                        <LineChart data={usersTimelineData}>
                          <CartesianGrid strokeDasharray="3 3" />
                          <XAxis dataKey="day" />
                          <YAxis width={20} />
                          <Tooltip />
                          <Legend />
                          {users.map((user, index) => (
                            <Line
                              key={user.id}
                              type="monotone"
                              dataKey={user.name}
                              stroke={lineColors[index % lineColors.length]}
                            />
                          ))}
                        </LineChart>
                      </ResponsiveContainer>
                    </Col>
                  </>
                )}
              </Row>
            </Col>
          )}
        </Row>
      </Container>
    );
  }
}
export default Visualize;

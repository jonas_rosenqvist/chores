import { Container, Row, Col } from "react-bootstrap";
import React from "react";

export const renderLoading = (msg: string = "Loading..") => {
  return (
    <Container>
      <Row>
        <Col xs={12}><h5 className="mt-3" style={{color: "#999"}}>{msg}</h5></Col>
      </Row>
    </Container>
  );
};

export const renderError = (msg: string) => {
  return (
    <Container>
      <Row>
        <Col xs={12}>{msg}</Col>
      </Row>
    </Container>
  );
};
export const getPriorityOptions = () => {
  return (
    <>
      <option value="3">Frequently</option>
      <option value="2">Occasionally</option>
      <option value="1">Rarely</option>
      <option value="0">Never (hides it from registration)</option>
    </>
  );
};

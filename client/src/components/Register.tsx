import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { getChoreDefs, createChore } from "../GraphQL";
import {
  Container,
  Row,
  Col,
  Card,
  Form,
} from "react-bootstrap";
import SplitButton from "./SplitButton";
import { ChoreDef, Chore, ChoreAction } from "../types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Collapse } from "react-bootstrap";
import { User, PopupProps, NotificationLevel } from "../types";
import { renderLoading, renderError } from "./Snippets";
import moment from "moment";

type Props = {
  setLocation: (location: string) => void;
  showNotification: (msg: string, level?: NotificationLevel) => void;
  showPopup: (props: PopupProps) => void;
  hidePopup: () => void;
  user: User;
  loadingUser: boolean;
};
type State = {
  redirect: string;
  error: string | null;
  choreDefs: { [choreDefId: string]: ChoreDef }; //immutable choreDefs from db
  chores: { [choreDefId: string]: Chore }; //mapping of choreDef to savable chore instance
  expandedChoreDefs: { [id: string]: boolean }; //mapping of choreDefs that are currently expanded
  loading: boolean;
  registrationTime: string;
};

class Register extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: false,
      redirect: "",
      error: null,
      choreDefs: {},
      chores: {},
      expandedChoreDefs: {},
      registrationTime: "Now"
    };
  }
  async componentDidMount() {
    if (!this.props.user.id && !this.props.loadingUser) {
      this.setState({ redirect: "/users/" });
      return;
    }
    try {
      this.setState({ loading: true });
      const choreDefs = await getChoreDefs();
      const dict: { [choreDefId: string]: ChoreDef } = {};
      choreDefs.forEach(choreDef => {
        if (choreDef.priority > 0) {
          dict[choreDef.id] = choreDef;
        }
      });
      this.setState({ choreDefs: dict, loading: false });
    } catch (error) {
      this.setState({ error, loading: false });
    }
    this.props.setLocation("Register chore");
  }

  addChore = async (choreDefId: string, selectedTime: string) => {
    const chore = this.state.chores[choreDefId];
    switch (selectedTime) {
      case "Now":
        break;
      case "Yesterday":
        chore.time = moment(chore.time, "YYYY-MM-DD")
        .subtract(1, "day")
        .toDate()
        break;
        case "Two days ago":
        chore.time = moment(chore.time, "YYYY-MM-DD")
        .subtract(2, "day")
        .toDate()
        break;
        case "Three days ago":
        chore.time = moment(chore.time, "YYYY-MM-DD")
        .subtract(3, "day")
        .toDate()
        break;
        case "Last week":
        chore.time = moment(chore.time, "YYYY-MM-DD")
        .subtract(7, "day")
        .toDate()
        break;
        case "Last month":
        chore.time = moment(chore.time, "YYYY-MM-DD")
        .subtract(1, "month")
        .toDate()
        break;
    }
    await createChore(chore);
    this.props.showNotification("Chore registered");
    this.setState({
      expandedChoreDefs: {
        ...this.state.expandedChoreDefs,
        [choreDefId]: false
      }
    });
  };
  isSelectedChoreAction = (choreDefId: string, choreActionId: string) => {
    if (!this.state.chores[choreDefId]) {
      return false;
    }
    return (
      this.state.chores[choreDefId].choreActions.filter(
        action => action.id === choreActionId
      ).length > 0
    );
  };
  hasAnySelectedChoreActions = (choreDefId: string) => {
    if (!this.state.chores[choreDefId]) {
      return false;
    }
    return this.state.chores[choreDefId].choreActions.length > 0;
  };
  selectChoreAction = (
    selected: boolean,
    choreDefId: string,
    choreAction: ChoreAction
  ) => {
    let chore = this.state.chores[choreDefId];
    if (!chore) {
      chore = {
        id: "",
        time: new Date(),
        choreDef: this.state.choreDefs[choreDefId],
        choreActions: [],
        user: this.props.user
      };
    }
    if (selected) {
      chore.choreActions.push(choreAction);
    } else {
      chore.choreActions = chore.choreActions.filter(
        action => action.id !== choreAction.id
      );
    }
    this.setState({ chores: { ...this.state.chores, [choreDefId]: chore } });
  };
  renderChoreDef = (choreDef: ChoreDef) => {
    const isExpanded = this.state.expandedChoreDefs[choreDef.id];

    return (
      <Col xs={12} md={6} lg={4} key={choreDef.id}>
        <Card className="my-3" bg="light" key={choreDef.id}>
          <Card.Header>
            <Card.Title style={{ marginBottom: "-.25rem" }}>
              {choreDef.name}
              <FontAwesomeIcon
                className="float-right icon-color"
                icon={choreDef.icon}
              />
            </Card.Title>
          </Card.Header>
          <Card.Body
            style={{ cursor: "pointer" }}
            onClick={() =>
              this.setState({
                expandedChoreDefs: {
                  ...this.state.expandedChoreDefs,
                  [choreDef.id]: !this.state.expandedChoreDefs[choreDef.id]
                }
              })
            }
          >
            <b>({choreDef.availableChoreActions.length})</b>{" "}
            {choreDef.description || <i> No description </i>}
            <FontAwesomeIcon
              className="float-right"
              size="lg"
              icon={isExpanded ? "angle-up" : "angle-down"}
            />
          </Card.Body>
          <Collapse in={isExpanded}>
            <Card.Footer>
              <Form>
                {choreDef.availableChoreActions.map(
                  (choreAction: ChoreAction) => {
                    return (
                      <Fragment key={choreAction.id}>
                        <Form.Check
                          style={{ display: "inline-block" }}
                          type="checkbox"
                          id={choreAction.id}
                          checked={this.isSelectedChoreAction(
                            choreDef.id,
                            choreAction.id
                          )}
                          onChange={(e: any) =>
                            this.selectChoreAction(
                              e.target.checked,
                              choreDef.id,
                              choreAction
                            )
                          }
                        />
                        <label
                          style={{ display: "inline" }}
                          htmlFor={choreAction.id}
                        >
                          <b>{choreAction.name} </b>{" "}
                          <i>
                            {choreAction.description} ({choreAction.points}{" "}
                            points)
                          </i>
                        </label>
                        <hr />
                      </Fragment>
                    );
                  }
                )}
                <SplitButton
                  uniqueButtonId={choreDef.id}
                  selectValues={["Now", "Yesterday", "Two days ago", "Three days ago", "Last week", "Last month"]}
                  defaultValue={this.state.registrationTime}
                  disabled={!this.hasAnySelectedChoreActions(choreDef.id)}
                  onClick={(selectedTime: string) =>
                    this.addChore(choreDef.id, selectedTime)
                  }
                  buttonText="Register"
                />
              </Form>
            </Card.Footer>
          </Collapse>
        </Card>
      </Col>
    );
  };

  render() {
    const { error, choreDefs, redirect, loading } = this.state;
    if (redirect) {
      return <Redirect to={redirect} />;
    }
    if (loading) {
      renderLoading();
    }
    if (
      Object.entries(choreDefs).length === 0 &&
      choreDefs.constructor === Object
    ) {
      return renderLoading("No chore definitions have been created yet");
    }
    if (error) {
      return renderError(error);
    }
    return (
      <Container className="my-5">
        <Row>
          {Object.values(choreDefs).map(choreDef =>
            this.renderChoreDef(choreDef)
          )}
        </Row>
      </Container>
    );
  }
}
export default Register;

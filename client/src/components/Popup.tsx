import { Modal, Button } from "react-bootstrap";
import React from "react";
import { PopupProps } from "../types";
class Popup extends React.Component<PopupProps> {
  render() {
    const {
      show,
      title,
      msg,
      danger,
      okText,
      cancelText,
      onOK,
      onCancel
    } = this.props;
    return (
      <Modal show={show} onHide={onCancel}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{msg}</Modal.Body>
        <Modal.Footer>
          {cancelText && <Button className="btn btn-info" onClick={onCancel}>{cancelText}</Button>}
          <Button className={danger ? "btn-danger" : ""} onClick={onOK}>{okText}</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default Popup;

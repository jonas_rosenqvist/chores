import React from "react";
import {NotificationProps, NotificationLevel} from "../types"
type Props = {
    notificationProps: NotificationProps;
}
export default function Notify(props:Props) {
    const {msg, level} = props.notificationProps;
    if (!msg){
        return null;
    }
    const levelClass = level === NotificationLevel.INFO ? "" : level === NotificationLevel.WARNING ? " notification-warning" : " notification-error";
    return <div className={"notification" + levelClass}>{msg}</div>
}
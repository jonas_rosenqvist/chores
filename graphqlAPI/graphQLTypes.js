const {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLBoolean
} = require("graphql");

const {
  GraphQLDateTime
} = require("graphql-iso-date");

const ChoreStatType = new GraphQLObjectType({
  name: "ChoreStat2",
  description: "...",
  fields: () => ({
    time: {
      type: GraphQLDateTime,
      resolve: data => data.time,
    },
    user: {
      type: GraphQLString,
      resolve: data => data.user
    },
    userName: {
      type: GraphQLString,
      resolve: data => data.userName
    },
    choreDef: {
      type: GraphQLString,
      resolve: data => data.choreDef,
    },
    points: {
      type: GraphQLInt,
      resolve: data => data.points
    },
    day: {
      type: GraphQLString,
      resolve: data => data.day,
    }
  })
})
const ChoreDefType = new GraphQLObjectType({
  name: "ChoreDef",
  description: "...",

  fields: () => ({
    id: {
      type: GraphQLString,
      resolve: data => data.id || data._id
    },
    name: {
      type: GraphQLString,
      resolve: data => data.name
    },
    description: {
      type: GraphQLString,
      resolve: data => data.description
    },
    icon: {
      type: GraphQLString,
      resolve: data => data.icon
    },
    priority: {
      type: GraphQLInt,
      resolve: data => data.priority
    },
    availableChoreActions: {
      type: GraphQLList(ChoreActionType),
      resolve: data => data.availableChoreActions
    }
  })
});

const ChoreActionInputType = new GraphQLInputObjectType({
    name: "ChoreActionInput",
    fields: () => ({
      id: { type: GraphQLString },
      name: { type: GraphQLString },
      description: { type: GraphQLString },
      points: { type: GraphQLInt },
    })
  });

const ChoreActionType = new GraphQLObjectType({
  name: "ChoreAction",
  description: "...",

  fields: () => ({
    id: {
      type: GraphQLString,
      resolve: data => data.id || data._id
    },
    name: {
      type: GraphQLString,
      resolve: data => data.name
    },
    description: {
      type: GraphQLString,
      resolve: data => data.description
    },
    points: {
      type: GraphQLInt,
      resolve: data => data.points
    }
  })
});

const ChoreType = new GraphQLObjectType({
  name: "Chore",
  description: "...",

  fields: () => ({
    id: {
      type: GraphQLString,
      resolve: data => data.id || data._id
    },
    time: {
      type: GraphQLDateTime,
      resolve: data => data.time
    },
    choreDef: {
      type: ChoreDefType,
      resolve: data => data.choreDef
    },
    choreActions: {
      type: GraphQLList(ChoreActionType),
      resolve: data => data.choreActions
    },
    user: {
      type: UserType,
      resolve: data => data.user
    }
  })
});

const LocationType = new GraphQLObjectType({
  name: "Location",
  description: "...",

  fields: () => ({
    id: {
      type: GraphQLString,
      resolve: data => data.id || data._id
    },
    name: {
      type: GraphQLString,
      resolve: data => data.name
    }
  })
});

const ChoreInputType = new GraphQLInputObjectType({
    name: "ChoreInput",
    description: "...",
    fields: () => ({
        time: {
            type: GraphQLDateTime
          },
          choreDef: {
            type: GraphQLString
          },
          choreActions: {
            type: GraphQLList(GraphQLString)
          },
          user: {
            type: GraphQLString
          }
      })
})
const DeleteResponseType = new GraphQLObjectType({
  name: "DeleteResponse",
  description: "...",
  fields: () => ({
    ok: {
      type: GraphQLBoolean,
      resolve: data => data.ok
    },
    msg: {
      type: GraphQLString,
      resolve: data => data.msg
    }
  })
})
const UserType = new GraphQLObjectType({
  name: "User",
  description: "...",

  fields: () => ({
    id: {
      type: GraphQLString,
      resolve: data => data.id || data._id
    },
    name: {
      type: GraphQLString,
      resolve: data => data.name
    },
    picture: {
      type: GraphQLString,
      resolve: data => data.picture
    },
    email: {
      type: GraphQLString,
      resolve: data => data.email
    },
    deleted: {
      type: GraphQLBoolean,
      resolve: data => data.deleted
    }
  })
});

exports.ChoreActionType = ChoreActionType;
exports.ChoreDefType = ChoreDefType;
exports.ChoreType = ChoreType;
exports.LocationType = LocationType;
exports.UserType = UserType;
exports.ChoreActionInputType = ChoreActionInputType;
exports.ChoreInputType = ChoreInputType;
exports.ChoreStatType = ChoreStatType;
exports.DeleteResponseType = DeleteResponseType;
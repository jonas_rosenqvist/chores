const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull,
  GraphQLInt
} = require("graphql");
const { GraphQLDateTime } = require("graphql-iso-date");
const {
  ChoreDefType,
  ChoreActionType,
  ChoreType,
  ChoreActionInputType,
  ChoreInputType,
  LocationType,
  UserType,
  ChoreStatType,
  DeleteResponseType
} = require("./graphQLTypes.js");

const {
  getChoreDefs,
  updateChoreDef,
  createChoreDef
} = require("../database/choreDef");
const {
  getChoreActions,
  createChoreAction
} = require("../database/choreAction");
const {
  getChores,
  createChore,
  deleteChore,
  getChoreStats
} = require("../database/chore");
const { getLocations } = require("../database/location");
const {
  getUsers,
  createUser,
  updateUser,
  deleteUser
} = require("../database/user");

module.exports = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Query",
    description: "...",

    fields: () => ({
      choreDefs: {
        type: GraphQLList(ChoreDefType),
        resolve: (root, args, req) => {
          return getChoreDefs();
        }
      },
      choreActions: {
        type: GraphQLList(ChoreActionType),
        resolve: (root, args, req) => {
          return getChoreActions();
        }
      },
      choreStats: {
        type: GraphQLList(ChoreStatType),
        args: {
          userId: { type: GraphQLString },
          choreDefId: { type: GraphQLString },
          timeFrom: { type: GraphQLInt },
          timeTo: { type: GraphQLInt }
        },
        resolve: (root, args, req) => {
          return getChoreStats(
            args.userId,
            args.choreDefId,
            args.timeFrom,
            args.timeTo
          );
        }
      },
      chores: {
        type: GraphQLList(ChoreType),
        args: {
          userId: { type: GraphQLString },
          choreDefId: { type: GraphQLString },
          timeFrom: { type: GraphQLInt },
          timeTo: { type: GraphQLInt },
          limit: { type: GraphQLInt }
        },
        resolve: (root, args, req, ast) => {
          const requestedFields = {}
          ast.fieldNodes[0].arguments.map(arg => {
            requestedFields[arg.name.value] = true;
          })
          return getChores(
            args.userId,
            args.choreDefId,
            args.timeFrom,
            args.timeTo,
            args.limit,
            requestedFields,
          );
        }
      },
      locations: {
        type: GraphQLList(LocationType),
        resolve: (root, args, req) => {
          return getLocations();
        }
      },
      users: {
        type: GraphQLList(UserType),
        args: {
          id: { type: GraphQLString }
        },
        resolve: (root, args, req) => {
          return getUsers(args.id);
        }
      }
    })
  }),
  mutation: new GraphQLObjectType({
    name: "Mutation",
    description: "...",
    fields: () => ({
      deleteChore: {
        type: DeleteResponseType,
        args: {
          id: {
            type: GraphQLString
          }
        },
        resolve: (root, args, req) => {
          return deleteChore(args.id);
        }
      },
      createChoreDef: {
        type: ChoreDefType,
        args: {
          name: {
            type: GraphQLString
          },
          description: {
            type: GraphQLString
          },
          icon: {
            type: GraphQLString
          },
          priority: {
            type: GraphQLInt
          },
          availableChoreActions: {
            type: GraphQLList(ChoreActionInputType)
          }
        },
        resolve: (root, args, req) => {
          return createChoreDef(
            args.name,
            args.description,
            args.icon,
            args.priority,
            args.availableChoreActions
          );
        }
      },

      updateChoreDef: {
        type: ChoreDefType,
        args: {
          id: {
            type: GraphQLString
          },
          name: {
            type: GraphQLString
          },
          description: {
            type: GraphQLString
          },
          icon: {
            type: GraphQLString
          },
          priority: {
            type: GraphQLInt
          },
          availableChoreActions: {
            type: GraphQLList(ChoreActionInputType)
          }
        },
        resolve: (root, args, req) => {
          return updateChoreDef(
            args.id,
            args.name,
            args.description,
            args.icon,
            args.priority,
            args.availableChoreActions
          );
        }
      },

      createChore: {
        type: ChoreType,
        args: {
          time: {
            type: GraphQLInt
          },
          choreDef: {
            type: GraphQLString
          },
          choreActions: {
            type: GraphQLList(GraphQLString)
          },
          user: {
            type: GraphQLString
          }
        },
        resolve: (root, args, req) => {
          return createChore(
            args.time,
            args.choreDef,
            args.choreActions,
            args.user
          );
        }
      },
      createChoreAction: {
        type: ChoreActionType,
        args: {
          name: {
            type: GraphQLString
          },
          description: {
            type: GraphQLString
          },
          points: {
            type: GraphQLInt
          }
        },
        resolve: (root, args, req) => {
          return createChoreAction(args.name, args.description, args.points);
        }
      },
      createUser: {
        type: UserType,
        args: {
          name: {
            type: GraphQLNonNull(GraphQLString)
          },
          email: {
            type: GraphQLString
          },
          picture: {
            type: GraphQLString
          }
        },
        resolve: (root, args, req) => {
          return createUser(args.name, args.email, args.picture);
        }
      },
      deleteUser: {
        type: DeleteResponseType,
        args: {
          id: {
            type: GraphQLString
          },
        },
        resolve: (root, args, req) => {
          return deleteUser(args.id);
        }
      },
      updateUser: {
        type: UserType,
        args: {
          id: {
            type: GraphQLNonNull(GraphQLString)
          },
          name: {
            type: GraphQLNonNull(GraphQLString)
          },
          email: {
            type: GraphQLString
          },
          picture: {
            type: GraphQLString
          }
        },
        resolve: (root, args, req) => {
          return updateUser(args.id, args.name, args.picture, args.email);
        }
      }
    })
  })
});

var mongoSchema = require("./mongoSchema");
var mongoose = require("mongoose");

const deleteChore = async id => {
  try {
    const result = await mongoSchema.Chore.deleteOne({ _id: id });
    return { ok: result.deletedCount === 1, msg: "" };
  } catch (err) {
    console.error("Couldn't delete chore: " + err);
    return { ok: false, msg: "500" };
  }
};
/**
 * @param {*} userId If present, limit search to a specific users. Deleted users are never included
 * @param {*} choreDefId If present, limit search to a specific choreDef
 * @param {*} timeFromInMillis only include chores registered after this
 * @param {*} timeToInMillis  only include chores registered before this
 */
const getChores = async (
  userId,
  choreDefId,
  timeFromInMillis,
  timeToInMillis,
  limit,
  requestedFields //[fieldname]: boolean map, might be useful in future optimizations
) => {
  const andConditions = [
    {
      time: {
        $lte: new Date(timeToInMillis * 1000),
        $gte: new Date(timeFromInMillis * 1000)
      }
    },
    { "user.deleted": false }
  ];
  if (userId) {
    andConditions.push({ "user._id": mongoose.Types.ObjectId(userId) });
  }
  if (choreDefId) {
    andConditions.push({ "choreDef._id": mongoose.Types.ObjectId(choreDefId) });
  }

  return await mongoSchema.Chore.aggregate([
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "joinedUser"
      }
    },
    {
      $lookup: {
        from: "choreactions",
        localField: "choreActions",
        foreignField: "_id",
        as: "joinedChoreActions"
      }
    },
    {
      $lookup: {
        from: "choredefs",
        localField: "choreDef",
        foreignField: "_id",
        as: "joinedChoreDef"
      }
    },
    {
      $project: {
        _id: 1,
        time: 1,
        user: { $arrayElemAt: ["$joinedUser", 0] },
        choreDef: { $arrayElemAt: ["$joinedChoreDef", 0] },
        choreActions: "$joinedChoreActions"
      }
    },

    //  { $unwind : "$joinedChoreActions" },
    {
      $match: {
        $and: andConditions
      }
    },
    { $sort: { time: -1 } },
    { $limit: limit }
  ]);
};
/**
 * Returns a list of records, where each record has:
 * {userId: user.id,
 * choreDefId: choreDef.id
 * time: chore.time
 * points: sum(chore.choreActions.points)}
 *
 * @param {*} userId If present, limit search to a specific users. Deleted users are never included
 * @param {*} choreDefId If present, limit search to a specific choreDef
 * @param {*} timeFromInMillis only include chores registered after this
 * @param {*} timeToInMillis  only include chores registered before this
 */
const getChoreStats = async (
  userId,
  choreDefId,
  timeFromInMillis,
  timeToInMillis
) => {
  //this should work after unwinding actions, because strangely enough
  //its applied to consequtive objects, not lists
  //{ $sum: points }
  //so, select only action based on time ONLY, unwind, sum, project.
  //no need to look up user or choredef, only need their ids
  const andConditions = [
    {
      time: {
        $lte: new Date(timeToInMillis * 1000),
        $gte: new Date(timeFromInMillis * 1000)
      }
    },
    {
      "user.deleted": {
        $ne: true
      }
    }
  ];
  if (userId) {
    andConditions.push({ user: mongoose.Types.ObjectId(userId) });
  }
  if (choreDefId) {
    andConditions.push({ choreDef: mongoose.Types.ObjectId(choreDefId) });
  }
  //todo: add user.name and match on !user.deleted.
  return await mongoSchema.Chore.aggregate([
    {
      //join choreActions
      $lookup: {
        from: "choreactions",
        localField: "choreActions",
        foreignField: "_id",
        as: "joinedChoreActions"
      }
    },
    {
      //  join user
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "joinedUser"
      }
    },
    {
      //filter on user-specified time interval, user and choredef
      $match: {
        $and: andConditions
      }
    }, //unwind choreActions
    { $unwind: "$joinedChoreActions" },
    {
      //only keep 'points' from choreActions, and get first user
      $project: {
        _id: 1,
        time: 1,
        user: 1,
        joinedUser: { $arrayElemAt: ["$joinedUser", 0] },
        choreDef: 1,
        points: "$joinedChoreActions.points"
      }
    },
    {
      //extract username
      $project: {
        _id: 1,
        time: 1,
        user: 1,
        userName: "$joinedUser.name",
        choreDef: 1,
        points: 1
      }
    },
    {
      //group together all documents with the same choreId, summing the points
      $group: {
        _id: "$_id",
        time: { $first: "$time" },
        user: { $first: "$user" },
        userName: { $first: "$userName" },
        choreDef: { $first: "$choreDef" },
        points: { $sum: "$points" }
      }
    },
    {
      //group together all documents with the same user AND same day, summing the points
      $group: {
        _id: {
          user: "$user",
          date: { $dateToString: { format: "%Y-%m-%d", date: "$time" } }
        },
        userName: { $first: "$userName" },
        time: { $first: "$time" },
        user: { $first: "$user" },
        choreDef: { $first: "$choreDef" },
        points: { $sum: "$points" }
      }
    },
    {
      //cleanup
      $project: {
        _id: 0,
        time: 1,
        day: "$_id.date",
        user: 1,
        userName: 1,
        choreDef: 1,
        points: 1
      }
    }, //sort by user, then time
    { $sort: { user: 1, time: -1 } }
  ]);
};

const clear = async () => {
  try {
    await mongoSchema.Chore.deleteMany({});
  } catch (err) {
    console.error("Couldn't clear chore: " + err);
  }
};

const createChore = async (timeInMillis, choreDef, choreActions, user) => {
  const time = new Date(timeInMillis * 1000);
  try {
    const mongoChore = new mongoSchema.Chore({
      time,
      choreDef,
      choreActions,
      user
    });
    const chore = await mongoChore.save();
    console.log("Created chore " + chore.id);
    return chore;
  } catch (err) {
    console.error("Couldn't add chore: " + err);
  }
};
exports.getChores = getChores;
exports.clear = clear;
exports.createChore = createChore;
exports.deleteChore = deleteChore;
exports.getChoreStats = getChoreStats;

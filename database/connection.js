var mongoose = require("mongoose");
dotenv = require("dotenv");
dotenv.config();
var baseData = require("./baseData");
const connect = () => {
  const mongoURL = process.env.MONGODB_URI || process.env.MONGO
  if (mongoURL.includes("heroku")){
    console.log("Running against the production database")
  }
  console.log("DB URL: " + mongoURL);
  mongoose.connect(mongoURL, {
    useFindAndModify: false,
    useNewUrlParser: true
  });
  var db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));

  db.once("open", function() {
    initdb();
  });
};

const initdb = async () => {
  const test = false;
  const clear = false;
  if (clear) {
    await baseData.clear();
    await baseData.init();
  }

  if (test && clear){
    await baseData.genTestData();
  }
  console.log("Finished:" + (clear ? " [clear]" : " ")  + (test ? " [test]" : ""))
};

module.exports = connect;

var mongoSchema = require("./mongoSchema");

const getChoreActions = async () => {
    try {
      return await mongoSchema.ChoreAction.find();
     
    } catch (err) {
      console.error("Couldn't get choreAction: " + err);
    }
  };

  const clear = async () => {
    try{
      await mongoSchema.ChoreAction.deleteMany({});
      return true;
    }catch(err){
       console.error("Couldn't clear ChoreAction: " + err);
    }
  }

  const createChoreAction = async (name, description, points) => {
    const mongoChoreDef = new mongoSchema.ChoreAction({name, description, points});
    try {
        return await mongoChoreDef.save();
      } catch (err) {
        console.error("Couldn't add ChoreAction: " + err);
      }
  }
exports.getChoreActions = getChoreActions;
exports.clear = clear;
exports.createChoreAction = createChoreAction;
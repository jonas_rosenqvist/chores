var mongoSchema = require("./mongoSchema");
const path = require('path');
const fs = require('fs');

const getUsers = async (id) => {
  const filter = {}
  if (id){
    filter["_id"] = id;
  }
  filter["deleted"] = {$ne: true};
    try {
      const users = await mongoSchema.User.find(filter);
      return users; 
     
    } catch (err) {
      console.error("Couldn't get user: " + err);
    }
  };

  const clear = async () => {
    try{
      await mongoSchema.User.deleteMany({});
    }catch(err){
      console.error("Couldn't clear user: " + err);
    }
  }

  const createUser = async (name, email, picture) => {
    const mongoUser = new mongoSchema.User({name, picture, email});
    try {
        const user = await mongoUser.save();
        if (picture.startsWith("profile_pending")){
          const newName = "profile_" + user.id +  path.extname(picture);
          fs.rename("public/images/" + picture, "public/images/" + newName, async function (err) {
            if (err){
              console.log(err);
            }
            return await mongoSchema.User.findOneAndUpdate(
              { _id: user.id },
              { $set: {picture: newName} },
              { new: true }
            );
          });
        }
      } catch (err) {
        console.error("Couldn't add user: " + err);
      }
  }
  const deleteUser = async id => {
    try {
      const result =  await mongoSchema.User.findOneAndUpdate({ _id: id }, {$set: {deleted: true}}); 
      return {ok: true, msg: ""}
    } catch (err) {
      console.error("Couldn't delete user: " + err);
      return {ok: false, msg: "500"}
    }
  }

  const updateUser = async (id, name, picture, email) => {

    const filter = {};
    if (name) {
      filter["name"] = name;
    }
    if (picture) {
      filter["picture"] = picture;
    }
    if (email) {
      filter["email"] = email;
    }
    try{
      return await mongoSchema.User.findOneAndUpdate(
        { _id: id },
        { $set: filter },
        { new: true }
      );
    }catch(err){
      console.log("Couldn't update user: " + err)
    }
    
  }
exports.getUsers = getUsers;
exports.clear = clear;
exports.createUser = createUser;
exports.updateUser = updateUser;
exports.deleteUser = deleteUser;
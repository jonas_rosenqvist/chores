var mongoose = require("mongoose");

const choreDefSchema = new mongoose.Schema({
  name: String,
  description: String,
  icon: String,
  priority: Number,
  availableChoreActions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "ChoreAction"
  }]
});
const choreActionSchema = new mongoose.Schema({
  name: String,
  description: String,
  points: Number,
});

const choreSchema = new mongoose.Schema({
  time: Date,
  choreDef: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "ChoreDef"
  },
  choreActions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "ChoreAction"
  }],
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
});

const locationSchema = new mongoose.Schema({
  name: String
});
const userSchema = new mongoose.Schema({
  name: String,
  picture: String,
  email: String,
  deleted: {
    type: Boolean,
    default: false,
  }
});

const ChoreDef = mongoose.model("ChoreDef", choreDefSchema);
const ChoreAction = mongoose.model("ChoreAction", choreActionSchema);
const Chore = mongoose.model("Chore", choreSchema);
const Location = mongoose.model("Location", locationSchema);
const User = mongoose.model("User", userSchema);

exports.ChoreDef = ChoreDef;
exports.ChoreAction = ChoreAction;
exports.Chore = Chore;
exports.Location = Location;
exports.User = User;

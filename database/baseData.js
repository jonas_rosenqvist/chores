const ChoreAction = require("./choreAction");
const ChoreDef = require("./choreDef");
const Chore = require("./chore");
const Location = require("./location");
const User = require("./user");

/**Only creates locations, and only if there are none */
const init = async () => {
  const locations = await Location.getLocations();
  if (!locations.length) {
    await Promise.all([
      Location.addLocation("Kitchen"),
      Location.addLocation("Bedroom"),
      Location.addLocation("Guest Room"),
      Location.addLocation("Bathroom"),
      Location.addLocation("Living Room"),
      Location.addLocation("Hallway"),
      Location.addLocation("Balcony"),
      Location.addLocation("Walk-in Closet"),
      Location.addLocation("Basement"),
      Location.addLocation("Laundry Room")
    ]);
  }
};
const clear = async () => {
  await Promise.all([
    ChoreDef.clear(),
    ChoreAction.clear(),
    Chore.clear(),
    Location.clear(),
    User.clear()
  ]);
};
/**
 * Creates users and choredefs with associated choreActions, unconditionally
 */
const genTestData = async () => {
  await Promise.all([
    User.createUser("Jonas", "rosenqvist.jonas@gmail.com", ""),
    User.createUser("Lucy", "lucinda.david@gmail.com", "")
  ]);

  /**
   * Cleaning
   */
  const cleaningAction1 = await ChoreAction.createChoreAction(
    "Living room",
    "Sweeping floors",
    5
  );
  const cleaningAction2 = await ChoreAction.createChoreAction(
    "Bedroom",
    "Sweeping floor and making the bed",
    10
  );
  const cleaningAction3 = await ChoreAction.createChoreAction(
    "Bathroom",
    "Cleaning toilet bowl and sink. Sweeping the floor",
    20
  );

  const cleaningChoreDef = await ChoreDef.createChoreDef(
    "Cleaning",
    "Various cleaning chores",
    "broom",
    1,
    [cleaningAction1.id, cleaningAction2.id, cleaningAction3.id]
  );

  /**
   * TRASH
   */

  const trashAction = await ChoreAction.createChoreAction(
    "Taking out trash",
    "Combustable, compost, paper, cardboard, glass, metal",
    5
  );

  const trashContainerCleaningAction = await ChoreAction.createChoreAction(
    "Cleaning containers",
    "Rinsing/washing all trash containers, replacing bags",
    5
  );

  const trashChoreDef = await ChoreDef.createChoreDef(
    "Trash",
    "Various trash related chores",
    "trash",
    1,
    [trashAction.id, trashContainerCleaningAction.id]
  );

  /**
   * Cooking
   */
  const cookingForTwoAction = await ChoreAction.createChoreAction(
    "Cooking for two",
    "Preparing a meal for two. Assumes partial cleanup as one cooks",
    20
  );
  const cookingForFourAction = await ChoreAction.createChoreAction(
    "Cooking for four.",
    "Preparing a meal for four. Assumes partial cleanup as one cooks",
    30
  );
  const cleanupCookingForTwoAction = await ChoreAction.createChoreAction(
    "Cleanup for two.",
    "Some pots and pans. Plates and cutlery. Putting away condiments and wiping down",
    10
  );
  const cleanupCookingForFourAction = await ChoreAction.createChoreAction(
    "Cleanup for four",
    "Some pots and pans. Plates and cutlery. Putting away condiments and wiping down",
    15
  );

  const cookingChoreDef = await ChoreDef.createChoreDef(
    "Cooking",
    "Chores related to cooking",
    "utensils",
    1,
    [
      cookingForTwoAction.id,
      cookingForFourAction.id,
      cleanupCookingForTwoAction.id,
      cleanupCookingForFourAction.id
    ]
  );

  /**
   * LAUNDRY
   */
  const laundryOneAction = await ChoreAction.createChoreAction(
    "One load",
    "Refers to common landry: bedsheets, towels etc.",
    5
  );
  const laundryTwoAction = await ChoreAction.createChoreAction(
    "Two loads",
    "Refers to common landry: bedsheets, towels etc.",
    7
  );
  const laundryCeanupAction = await ChoreAction.createChoreAction(
    "Putting away one load",
    "Refers to common landry, not including remaking the bed",
    5
  );
  const laundryRemakeBedAction = await ChoreAction.createChoreAction(
    "Remake the bed",
    "Stripping and redressing bed: sheets, pillows cases and duvet cases",
    5
  );

  const choreDef3 = await ChoreDef.createChoreDef("Laundry", "Laundry and beddings", "tshirt", 1, [
    laundryOneAction.id,
    laundryTwoAction.id,
    laundryCeanupAction.id,
    laundryRemakeBedAction.id
  ]);

  /**
   * Shopping
   */
  const groceryShoppingAction = await ChoreAction.createChoreAction(
    "Grocery shopping",
    "One trip to a grocery store",
    10
  );

  const groceryShoppingChoreDef = await ChoreDef.createChoreDef(
    "Shopping",
    "Shopping chores",
    "shopping-basket",
    1,
    [groceryShoppingAction.id]
  );

    /**
   * Cat
   */
  const cleanLitterboxAction = await ChoreAction.createChoreAction(
    "Clean litter box",
    "",
    5
  );

  const refillLitterboxAction = await ChoreAction.createChoreAction(
    "Refill litter box",
    "",
    5
  );
  catChoreDef = await ChoreDef.createChoreDef(
    "Cat",
    "Cat related chores",
    "cat",
    1,
    [cleanLitterboxAction.id, refillLitterboxAction.id]
  );

  /**
   * Dishwasher
   */
  const emptyDishwasherAction = await ChoreAction.createChoreAction(
    "Empty dish washer",
    "",
    5
  );
  catChoreDef = await ChoreDef.createChoreDef(
    "Dishwasher",
    "Chores related to washing dishes",
    "utensils",
    1,
    [emptyDishwasherAction.id]
  );
};



exports.init = init;
exports.clear = clear;
exports.genTestData = genTestData;

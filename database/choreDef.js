var mongoSchema = require("./mongoSchema");

const getChoreDefs = async () => {
  try {
    return await mongoSchema.ChoreDef.find().populate({
      path: "availableChoreActions"
    });
  } catch (err) {
    console.error("Couldn't get choreDefs: " + err);
  }
};

const clear = async () => {
  try {
    await mongoSchema.ChoreDef.deleteMany({});
  } catch (err) {
    console.error("Couldn't clear choreDef: " + err);
  }
};

const updateChoreDef = async (
  id,
  name,
  description,
  icon,
  priority,
  availableChoreActions
) => {
  const filter = {};
  if (name) {
    filter["name"] = name;
  }
  if (description) {
    filter["description"] = description;
  }
  if (icon) {
    filter["icon"] = icon;
  }
  if (Number.isInteger(priority)) {
    filter["priority"] = priority;
  }
  if (availableChoreActions) {
    filter["availableChoreActions"] = availableChoreActions.map(action => action.id);
  }
  availableChoreActions.map(async action => {
    const { id, name, description, points } = action;
    try{
      const updatedChoreAction = await mongoSchema.ChoreAction.findOneAndUpdate(
        { _id: id },
        { $set: { name, description, points } },
        { upsert: true, new: true }
      );
      
      console.log(updatedChoreAction);
    }catch(err){
      console.log("Couldn't update choreAction: " + err)
    }
  });
  try{
    const updatedChoreDef = await mongoSchema.ChoreDef.findOneAndUpdate(
      { _id: id },
      { $set: filter },
      { new: true }
    );
    console.log(updatedChoreDef);
    return updatedChoreDef;
  }catch(err){
    console.log("Couldn't update choreDef: " + err);
  }

};

const createChoreDef = async (
  name,
  description,
  icon,
  priority,
  availableChoreActions
) => {
  const mongoCategory = new mongoSchema.ChoreDef({
    name,
    description,
    icon,
    priority,
    availableChoreActions: (availableChoreActions ? availableChoreActions : [])
  });
  try {
    return await mongoCategory.save();
  } catch (err) {
    console.error("Couldn't add choreDef: " + err);
  }
};
exports.getChoreDefs = getChoreDefs;
exports.clear = clear;
exports.createChoreDef = createChoreDef;
exports.updateChoreDef = updateChoreDef;

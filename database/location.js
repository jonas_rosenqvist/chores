var mongoSchema = require("./mongoSchema");

const getLocations = async () => {
    try {
      return locations = await mongoSchema.Location.find();
     
    } catch (err) {
      console.error("Couldn't get location: " + err);
    }
  };

  const clear = async () => {
    try{
      await mongoSchema.Location.deleteMany({});
    }catch(err){
      console.error("Couldn't clear chore: " + err);
    }
  }

  const addLocation = async (name) => {
    const mongoLocation = new mongoSchema.Location({name});
    try {
        return await mongoLocation.save();
      } catch (err) {
        console.error("Couldn't add location: " + err);
      }
  }
exports.getLocations = getLocations;
exports.clear = clear;
exports.addLocation = addLocation;